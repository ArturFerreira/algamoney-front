import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarraNavegacaoComponent } from './barra-navegacao/barra-navegacao.component';

import { DatePipe } from '@angular/common';

import { ButtonModule } from 'primeng/button';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { ErrorHandlerService } from './error-handler.service';

import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { RouterModule} from "@angular/router";


registerLocaleData(localePt);
@NgModule({
  declarations: [
    BarraNavegacaoComponent
  ],
    imports: [
        CommonModule,
        ButtonModule,
        RouterModule,
    ],
  providers: [
    DatePipe, MessageService, ConfirmationService, ErrorHandlerService, {provide: LOCALE_ID, useValue: 'pt-BR'}
  ],
  exports:[
    BarraNavegacaoComponent,
    ToastModule,
    ConfirmDialogModule,
  ]
})
export class CoreModule { }
