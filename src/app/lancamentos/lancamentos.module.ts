import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import { SelectButtonModule } from 'primeng/selectbutton';
import { DropdownModule } from 'primeng/dropdown';
import { InputNumberModule } from 'primeng/inputnumber';

import { LancamentosPesquisaComponent } from './lancamentos-pesquisa/lancamentos-pesquisa.component';
import { LancamentosCadastroComponent } from './lancamentos-cadastro/lancamentos-cadastro.component';
import {RouterLink} from "@angular/router";


@NgModule({
  declarations: [
    LancamentosCadastroComponent,
    LancamentosPesquisaComponent
  ],
    imports: [
        FormsModule,
        CommonModule,
        TableModule,
        CalendarModule,
        InputTextModule,
        ButtonModule,
        ToolbarModule,
        TooltipModule,
        DropdownModule,
        InputNumberModule,
        SelectButtonModule,
        SharedModule,
        RouterLink
    ],
  exports: [

  ]
})
export class LancamentosModule { }
